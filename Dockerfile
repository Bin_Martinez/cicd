FROM ubuntu:focal

ENV DEBIAN_FRONTEND noninteractive

LABEL MAINTAINER="Bin Martinez <bin.martinez@outlook.com>"
MAINTAINER Bin Martinez <bin.martinez@outlook.com>

RUN apt-get update 
RUN apt-get install -y openssh-client openssh-server sshpass  
RUN  apt-get clean
RUN rm -rf /var/lib/apt/lists/*
